# A pet project

# Requirements
  python 2.7 or later
  coverage.py from [here](https://coverage.readthedocs.io/en/v4.5.x/) - You can install this with
  `
  pip install coverage --user
  `

# Executing
From the 'src' folder:
`
python .
`

# Code Coverage
You can generate code coverage reports with this:

`
coverage run pets.ut.py && coverage html
`

This will create the `htmlcov` folder where you can browse the `index.html` file to see coverage statistics

# SQL
10) Now we want to persist the state of the cats and dogs to database tables. Write SQL that would create the schema necessary to do so. We need to store not only their current name and age, but also a historical record of previous names for each.

CREATE TABLE pet_type (
    id int not null auto_increment, 
    type_name varchar (20) not null unique,
    PRIMARY KEY (id));

CREATE TABLE pets (
    id int not null auto_increment, 
    pet_type int not null, 
    pet_name varchar(255) not null, 
    age int not null, PRIMARY KEY (id),
    CONSTRAINT fkey_pets FOREIGN KEY (pet_type) REFERENCES pet_type(id));

CREATE TABLE pet_names (
    id int not null auto_increment, 
    pet_id int not null, 
    pet_name varchar(25) not null, 
    PRIMARY KEY (id), 
    CONSTRAINT fkey FOREIGN KEY (pet_id) references pet_names(id));

# Theory
11) List some ways to abstract the dogs and cats in your program if you haven't already implemented those abstractions.

Cats and Dogs extend from a common Pet class, where nearly all of the logic resides. 

12) Explain your data modeling strategy for step #10. What performance optimizations did you make / could you make, and what trade-offs do those optimizations incur?

Assuming RDBMS (instead of NOSQL), specifically mysql (vs lighter weight hsqldb, or heavier postresql, etc.)  These restrictions make available AUTO_INCREMENT and short-hand primary key definitions, which is a bit of an optimization.  Using custom syntax, instead of strict ANSI-92 syntax, can lock you to a particular RDBMS implementation, which is objectionable to many developers.

Using 1N normalization to prevent duplicate data and make queries faster.  Trade-Offs?  Only that you have to make a more complex query to retrieve all of the data associated with a pet.
