import unittest
from pets import Cat, Dog, Pet

class TestPet(unittest.TestCase):
  def setUp(self):
    self.pet = Pet("name", 1)

  def test_name_is_set(self):
    self.assertEqual(self.pet.name, "name")
  
  def test_age_is_set(self):
    self.assertEqual(self.pet.age(), 1)

  def test_name_reset(self):
    self.pet.name = "New Name"
    self.assertEqual(self.pet.name, "New Name")
    self.assertTrue(len(self.pet._names) == 2)

  def test_every_second_speak_increments_age(self):
    said = self.pet.speak("say something")
    self.assertTrue(self.pet._speak_count == 1)
    self.assertTrue(self.pet.age(), 1)
    self.assertTrue(said == "say something")
    self.pet.speak("Say again?")
    self.assertTrue(self.pet._speak_count == 2)
    self.assertEqual(self.pet.age(), 2 )

  def test_speak_returns_None_by_default(self):
    said = self.pet.speak()
    self.assertTrue(said == None)

  def test_avg_name_len(self):
    self.assertTrue(self.pet.average_name_len() == 4)
    self.pet.name = "New Name"
    self.assertTrue(len(self.pet._names) == 2)
# Test step 6 - at the pet level 
    self.assertTrue(self.pet.average_name_len() == 6)
    self.pet.name = "Really long name"
# This just to show that it changes and that it is a decimal value
    self.assertTrue(self.pet.average_name_len() > 9.3 and self.pet.average_name_len() < 9.4)

  def test_dog_says_bow_wow(self):
    dog = Dog("Daze", 8)
    said = dog.speak()
    self.assertTrue(said == "bow-wow")

  def test_cat_says_meow_and_can_be_overridden(self):
    cat = Cat("Sigmund", 18)
    said = cat.speak()
    self.assertTrue(said == "meow")
    said = cat.speak("purr")
    self.assertTrue(said == "purr")
# Test step 5 - at the cat level - also applies to Dog since they have the common Pet ancestor
    self.assertTrue(cat.age() == 19) 

if __name__ == '__main__':
  unittest.main()
