class Pet(object):
  def __init__(self, name, age):
    self._name = name
    self._age = age
    self._names = list()
    self._speak_count = 0
    self._sound = None
    self._names.append(name)
    self._name_lens = len(name)
    self._default_sound = None

  @property
  def name(self):
    return self._name

  @name.setter
  def name(self, name):
    self._names.append(name)
    self._name = name
    self._name_lens += len(name)

  def age(self):
    return self._age

  def speak(self, optional = "use-default"):
    if optional == "use-default":
      retval = self._default_sound
    else:
      retval = optional
    self._speak_count += 1
    if self._speak_count % 2 == 0:
      self._age += 1
    return retval

  def average_name_len(self):
    return float(self._name_lens) / float(len(self._names))

class Dog(Pet):
  def __init__(self, name, age):
    Pet.__init__(self, name, age)
    self._default_sound = "bow-wow"

class Cat(Pet):
  def __init__(self, name, age):
    Pet.__init__(self, name, age)
    self._default_sound = "meow"
