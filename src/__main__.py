import random

from pets import Dog, Cat

dog = Dog("Santa's Little Helper", random.randint(1, 3))
cat = Cat("Snowball II", random.randint(3, 5))

print dog.name, "is currently", dog.age(), "years old."
print dog.name, "says", dog.speak("meow"), "? Nah,", dog.name, "says", dog.speak(), "!  Now", dog.name, "is", dog.age(), "years old."
print "The cat's name is", cat.name
cat.name = "Garfield"
print "The cat's name has been changed to ", cat.name, ". The average length of the cat's name is",cat.average_name_len()
